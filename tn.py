import signal
import os
import sys
import curses
import traceback
import atexit
import time
import thread


s=curses.initscr()
s.keypad(1)
d=s.getmaxyx()

def rn():
	s.nodelay(0)
	selection=-1
	option=0

	filename = "/pda/Notes/"
	dir = os.path.dirname(filename)
	try:
		os.stat(dir)
		os.system('ls /pda/Notes/> /pda/db/notels.txt')
		text_file = open("/pda/db/notels.txt", "r")
		lines = text_file.readlines()
		text_file.close()
		k=len(lines)
		f=0
		while selection<0:
			if f==0:
				os.system('echo "'+lines[0]+'" | flite' )
				f=1
			g=[0]*(k+1)
			g[option]=curses.A_REVERSE
			s.addstr(0,d[1]/2-3,'SELECT NOTE')	
			i=0
			for i in range(0,len(lines)):
				s.addstr(d[0]/2-8+i,d[1]/2-4,lines[i],g[i])
			
			s.addstr(d[0]/2-8+k,d[1]/2-4,'Back to Menu',g[k ])
			s.refresh()
			action=s.getch()

			if action == curses.KEY_UP:
				option=(option-1)%(k+1)
			elif action == curses.KEY_DOWN:
				option=(option+1)%(k+1)
			elif action == ord('\n'):
				selection=option
			
			for i in range(0,len(lines)+1):
				if (option==i)and (i<k):
					os.system('echo "'+lines[i]+'" | flite' )
				elif(option==i) and (i==k):
					os.system('echo "Back to menu" | flite' )


			for i in range(0,len(lines)):
				if i== len(lines)+1:
					menu.mf()
				elif selection == i :
					path='/pda/Notes/'+lines[selection]
					os.system('flite -f '+path ) 				
					
	except:
		try:
			os.mkdir(dir)
			os.system('echo "Nothing to read" | flite' )
		except:
			os.system('echo "Folder is empty" | flite' )
		
	s.clear()			


def tn():
	s.nodelay(0)
	selection=-1
	option=0

	filename = "/pda/Notes/"
	dir = os.path.dirname(filename)
	try:
		os.stat(dir)
	#	print "/pda/ReadFiles/ present"
	except:
		os.mkdir(dir)
	#	print "/pda/ReadFiles/ created"
	os.system('ls /pda/Notes/> /pda/db/notels.txt')
	text_file = open("/pda/db/notels.txt", "r")
	lines = text_file.readlines()
	text_file.close()
	ff=0
	os.system('echo "Enter note label" | flite' )
	f=0
	while f==0:
		nn = my_raw_input(s, 2, 3, "Enter note label ").lower()+'\n'
		s.clear()
		if nn in lines and f==0:
			f=1
			os.system('echo "File already exist try again" | flite' )
		elif f==0:
			f=1
			os.system('echo "enter containt" | flite' )
			note = my_raw_input(s, 2, 3, "Enter containt ").lower()
			os.system('ls /pda/Notes/> /pda/db/notels.txt')
			os.system('echo '+note+'> /pda/Notes/'+nn)
			os.system('echo "Note successfully saved" | flite' )

	s.clear()




def dn():
	s.nodelay(0)
	selection=-1
	option=0

	filename = "/pda/Notes/"
	dir = os.path.dirname(filename)
	try:
		os.stat(dir)
		os.system('ls /pda/Notes/> /pda/db/notels.txt')
		text_file = open("/pda/db/notels.txt", "r")
		lines = text_file.readlines()
		text_file.close()
		k=len(lines)
		f=0
		while selection<0:
			if f==0:
				os.system('echo "'+lines[0]+'" | flite' )
				f=1
			g=[0]*(k+1)
			g[option]=curses.A_REVERSE
			s.addstr(0,d[1]/2-3,'SELECT NOTE')	
			i=0
			for i in range(0,len(lines)):
				s.addstr(d[0]/2-8+i,d[1]/2-4,lines[i],g[i])
			
			s.addstr(d[0]/2-8+k,d[1]/2-4,'Back to Menu',g[k ])
			s.refresh()
			action=s.getch()

			if action == curses.KEY_UP:
				option=(option-1)%(k+1)
			elif action == curses.KEY_DOWN:
				option=(option+1)%(k+1)
			elif action == ord('\n'):
				selection=option
			
			for i in range(0,len(lines)+1):
				if (option==i)and (i<k):
					os.system('echo "'+lines[i]+'" | flite' )
				elif(option==i) and (i==k):
					os.system('echo "Back to menu" | flite' )


			for i in range(0,len(lines)):
				if i== len(lines)+1:
					sys.exit()
				elif selection == i :
					path='/pda/Notes/'+lines[selection]
					os.system('echo "removing file" | flite')
					os.system('sudo rm -f '+path )
	except:
		try:
			os.mkdir(dir)
			os.system('echo "Nothing to delete" | flite' )
		except:
			os.system('echo "Folder is empty" | flite' )
		
	s.clear()



def my_raw_input(s, r, c, prompt_string):
    curses.echo() 
    s.addstr(r, c, prompt_string)
    s.refresh()
    input = s.getstr(r + 1, c, 1000)
    return input
