
import signal
import os
import sys
import curses
import traceback
import atexit
import time


s=curses.initscr()
s.keypad(1)
d=s.getmaxyx()

def rfusb():
	s.nodelay(0)
	selection=-1
	option=0
	filename="/pda/USB/"
	os.system('sudo umount /dev/sda1')
	os.system('sudo mount /dev/sda1 /pda/USB -o uid=pi,gid=pi')
	dir = os.path.dirname(filename)
	try:
		os.stat(dir)
		os.system('ls "'+filename+'"> /pda/db/usbls.txt')
		text_file = open("/pda/db/usbls.txt", "r")
		lines = text_file.readlines()
		text_file.close()
		k=len(lines)
		f=0
		while selection<0:
			if f==0:
				os.system('echo "'+lines[0]+'" | flite' )
				f=1
			g=[0]*(k+1)
			g[option]=curses.A_REVERSE
			s.addstr(0,d[1]/2-3,'SELECT FILE')	
			i=0
			for i in range(0,len(lines)):
				s.addstr(d[0]/2-8+i,d[1]/2-4,lines[i],g[i])
			
			s.addstr(d[0]/2-8+k,d[1]/2-4,'Back to Menu',g[i+1])
			s.refresh()
			action=s.getch()

			if action == curses.KEY_UP:
				option=(option-1)%(k+1)
			elif action == curses.KEY_DOWN:
				option=(option+1)%(k+1)
			elif action == ord('\n'):
				selection=option
			
			for i in range(0,len(lines)+1):
				if (option==i)and (i<k):
					os.system('echo "'+lines[i]+'" | flite' )
				elif(option==i) and (i==k):
					os.system('echo "Back to menu" | flite' )


			for i in range(0,len(lines)):
				if i== len(lines)+1:
					sys.exit()
				elif selection == i:
					print ""				
		s.clear()
	except:
		print ""
	#	print "/pda/ReadFiles/ created"
	
			

def df():
	s.nodelay(0)
	selection=-1
	option=0

	filename = "/pda/ReadFiles/"
	dir = os.path.dirname(filename)
	try:
		os.stat(dir)
		os.system('ls /pda/ReadFiles/> /pda/db/ls.txt')
		text_file = open("/pda/db/ls.txt", "r")
		lines = text_file.readlines()
		text_file.close()
		k=len(lines)
		f=0
		while selection<0:
			if f==0:
				os.system('echo "'+lines[0]+'" | flite' )
				f=1
			g=[0]*(k+1)
			g[option]=curses.A_REVERSE
			s.addstr(0,d[1]/2-3,'SELECT NOTE')	
			i=0
			for i in range(0,len(lines)):
				s.addstr(d[0]/2-8+i,d[1]/2-4,lines[i],g[i])
			
			s.addstr(d[0]/2-8+k,d[1]/2-4,'Back to Menu',g[k ])
			s.refresh()
			action=s.getch()

			if action == curses.KEY_UP:
				option=(option-1)%(k+1)
			elif action == curses.KEY_DOWN:
				option=(option+1)%(k+1)
			elif action == ord('\n'):
				selection=option
			
			for i in range(0,len(lines)+1):
				if (option==i)and (i<k):
					os.system('echo "'+lines[i]+'" | flite' )
				elif(option==i) and (i==k):
					os.system('echo "Back to menu" | flite' )


			for i in range(0,len(lines)):
				if i== len(lines)+1:
					sys.exit()
				elif selection == i :
					path='/pda/ReadFiles/'+lines[selection]
					os.system('sudo rm -f '+path )
	except:
		try:
			os.mkdir(dir)
			os.system('echo "Nothing to delete" | flite' )
		except:
			os.system('echo "Folder is empty" | flite' )
		
	s.clear()

