import signal
import os
import sys
import curses
import traceback
import atexit
import time
import tn
import ReadFile
import Music
import usb
import ctu
import ScanAndRead
s=curses.initscr()
s.keypad(1)
d=s.getmaxyx()

class menu:
	def mf(self,para):
		s.nodelay(0)
		selection=-1
		option=0
		f=0
		menu_it=['Take Note','Read Notes','Remove Notes','Scan and Read','Scan and Save','Remove document','Read from USB','Read from PC','play music','delete music files','copy from USB','read from USB','Exit']
		while selection<0:
			if f==0:
				option=para
				os.system('echo "'+menu_it[para]+'" | flite' )
				f=1
			g=[0]*13
			g[option]=curses.A_REVERSE
			s.addstr(0,d[1]/2-3,'SELECT MENU')	
			for i in range(0,13):
				s.addstr(d[0]/2+i-7,d[1]/2-4,menu_it[i],g[i])
			s.refresh()
			action=s.getch()

			if action == curses.KEY_UP:
				option=(option-1)%13
			elif action == curses.KEY_DOWN:
				option=(option+1)%13
			elif action == ord('\n'):
				selection=option
			
			for i in range(0,13):
				if option==i:
					os.system('echo "'+menu_it[i]+'" | flite' )
			s.clear()

			if selection ==0:
				tn.tn()
				self.mf(0)
			elif selection == 1:
				tn.rn()
				self.mf(1)
			elif selection == 2:
				tn.dn()
				self.mf(2)
			elif selection == 3:
                                ScanAndRead.sar()
                                self.mf(3)
			elif selection == 4:
                                ScanAndRead.sas()
                                self.mf(4)
			elif selection == 5:
				ReadFile.df()
				self.mf(5)
			elif selection == 6:
				usb.rfusb()
				self.mf(6)
			elif selection == 7:
				ReadFile.rf()
				self.mf(7)
			elif selection == 8:
				Music.pm()
				self.mf(8)
			elif selection == 9:
				Music.dm()
				self.mf(9)
			elif selection == 10:
                                ctu.cfu()
                                self.mf(10)
			elif selection == 12:
				selection=1
				sys.exit()
				break
				

obj=menu()

try:
    chk=(os.path.isdir("/pda"))
    filename = "/pda/"
    dir = os.path.dirname(filename)
    try:
    	os.stat(dir)
    except:
    	os.mkdir(dir)
    	os.system('sudo mkdir /pda/db/')
    obj.mf(0)
except SystemExit:
    pass

curses.endwin()    	
